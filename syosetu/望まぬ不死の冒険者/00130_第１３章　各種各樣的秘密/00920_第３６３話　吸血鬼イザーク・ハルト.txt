所謂「分化」，據說能夠反映出吸血鬼的個性來

將自己的身體想像成其它東西，在這個「其它東西」上，每個人都是不同的
就像人類有自己的個性一樣，吸血鬼也是如此
但即便如此，變化的方嚮往往大同小異， 這也是無可爭議的事實

比方說我——伊薩克·哈爾特，我使用分化，就會成為蝙蝠，這種形象是很常見的
許多高位吸血鬼在分化時都會選擇蝙蝠，我也選擇了這種形象
原因在於，正如這次我把分化的方法教給別人一樣，一開始，所有人都是被誰教會的
那時候看到的示範，也會對之後自己的想像產生影響，這也是理所當然的事情

當然，也不是看到了蝙蝠就只能選擇蝙蝠了，基本上想像還是個人的自由，分化成其它東西也是很常見的

比如說這次的貓，其它動物我也曾經見過
總的來說，莉娜會變成貓也不是什麼好奇怪的事情
但以莉娜為例子，將來她嘗試進行全體分化的話，也一定會變成貓吧

所謂「分化」，就是這麼一回事。我也是一樣，無論進行何種程度的分化，產生的形象總會是蝙蝠


但是
但是啊

現在，發生在我眼前的現象算是怎麼一回事呢？

雷特
雷特·梵納

他首先將身體變成了植物
雖然這種形象十分罕見，但也不是什麼值得大書特書的事情
因為要分化成什麼，是個人的自由，那麼，就算變成植物也沒什麼奇怪的

不過，分化之後，能夠做出何種程度的動作，會在一定程度上受制於產生的形象，所以會選擇這種東西的人幾乎沒有

至少，我從來沒見過

儘管如此還是做出了這種選擇，雷特·梵納還真是個奇怪的人呢。嘛，僅僅這樣的話也沒什麼

但問題在於這之後


他可能在自己準備分化成植物後，才察覺到這種形象的不便之處吧
露出了慌張的神色
但是，最初的「分化」，是非常重要的，一旦印象固定下來，就很難再去改變

雖然也並非完全不可能，但一度分化成某種形象，之後使用分化時，就會自然而然變成相同的形象
想要改變這個，需要非常高超的技巧與長期的努力，所以，最初的分化，必須慎重進行

話雖如此，這方面的意識太強，搞不好會導致分化本身失敗，所以保持適度的緊張感即可
正因為這樣，讓作為老師的人物展現自己的「分化」，在某種程度上留下印象之後，就會讓學生們照著這種感覺，趁熱打鐵進行嘗試

因此，就如剛才所說的一樣，很多人都會選擇蝙蝠
作為例外，選擇其他動物的人也偶有出現，但在比這更加奇怪選擇就很少見了


可是雷特·梵納卻......

在部分轉變為植物後，想要進一步分化出其它形象來

然後，有某種東西飛了出來
定睛細看，是鳥與兔子的形象

兩者都是黑影，與通常的分化一樣......但是，隨著它們在雷特周圍來回跑動，身體細節逐漸變得清晰起來，變得就像是活物一樣

在這之上，雷特的身體中還在不斷湧出其它形象

豬、小型的龍、老鼠等等......而且，雷特的身體也開始逐漸扭曲、變成深綠色，膨脹、扭曲著，宛如邪惡森林一般
而且體積膨大到了原來身體的數倍，那副模樣，讓人聯想起之前施米尼變成的魔物
說實話，這難道不是失敗了麼？

一瞬間，我猶豫著要不要出手阻止

但是，跑來跑去的各種生物，不久之後開始返回森林
然後，隨著動物們的回歸，森林也停止了繼續膨脹，逐漸縮小起來


深綠色的森林，逐漸變回黑影，然後又化作人形
只是，那人形中似乎有許多動物一樣，時不時有頭或腳伸出來，但那也漸漸平息了
然後，黑色的人形慢慢浮現出色彩——最後變回了雷特·梵納的形象

沒有失敗麼......

就是說，雷特·梵納已經完全控制了這些

第一次分化，出現了那種混沌的形象，生出了幾個，幾十個身體，視點和意識都互相分離，然後又將這一切統合起來，恢復了原狀

這是多麼困難的事情，若不是完全掌握了「分化」能力的吸血鬼，是想像不出的吧
也存在著一小部分吸血鬼的精英，一開始就能掌握複雜的分化，但我對雷特完全沒有抱那種期待，所以這令我大吃一驚

話雖如此，他的實力也並非特別優秀，感覺只是技能方面有些微妙......
怎麼形容呢？用魔術和氣來打比方的話，就是擅長細微的控制，與魔力和氣本身非常強大是不同的

當然，控制力也是非常重要的東西，所以這一點也值得讚賞
至於雷特的力量有多大，今後將如何發展，還是個未知數。但值得期待這一點，是毋庸置疑的

只是，剛才的那個「分化」，該怎麼解釋呢......

果然，是因為雷特先生並非普通吸血鬼麼？
但那樣的話，莉娜也應該是同類。可她的分化，雖然有些特殊，還是能算在一般範疇內的
那麼，還有其他理由嗎？
想不出來啊......


「......呼，總算是回來了。怎麼樣啊，伊薩克？我做得還算不錯吧」

完全沒有意識到我內心的種種困惑，雷特這樣搭話了過來
我並不知道他的問題該如何回答
但是，關於那個「分化」做的如何，還是可以說上兩句的

「......哎，做的不錯。但是，那種奇妙的「分化」，一般人不可能做得出吧」

說出的話稍微有些否定之意，這是我心中糾葛的表現
但在這個回答上，我並沒有需要被責怪的點